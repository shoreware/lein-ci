(ns leiningen.ci
  "Interact with the continuous integration service."
  (:require [clojure.string]
            [bultitude.core :as b]
            [jenkins.core :as j]
            [leiningen.core.eval :as eval]
            [leiningen.core.main :as main]))

(def supported-services (atom [:jenkins]))

(defn which-ci [project & _]
  (or (:ci project) (first @supported-services)))


;;; Methods

(defmulti build "Trigger a job on remote server."
  which-ci :default :none)



;;; CI not found

(defn- unknown-ci [task]
  (binding [*out* *err*]
  (println (str "Unknown CI detected for 'ci " task "'")))
  (System/exit 1))

(defmethod build :none [project & [args]] (unknown-ci "build"))



;;; Jenkins

(defmethod build :jenkins [project job-name]
  (j/with-config (:jenkins-ci project)
    (j/build job-name)))


(defn- not-found [subtask]
  (partial #'main/task-not-found (str "ci " subtask)))

(defn- load-methods []
  (doseq [n (b/namespaces-on-classpath :prefix "leiningen.ci.")]
    (swap! supported-services conj (keyword (last (.split (name n) "\\."))))
    (require n)))

(defn ^{:subtasks [#'build]} ci
  "Interact with the continuous integration server."
  [project subtask & args]
  (load-methods)
  (let [subtasks (:subtasks (meta #'ci) {})
        [subtask-var] (filter #(= subtask (name (:name (meta %)))) subtasks)]
    (apply (or subtask-var (not-found subtask)) project args)))
