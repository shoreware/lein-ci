; Stolen from https://github.com/owainlewis/jenkins
(ns jenkins.core
  (:require [clj-http.client :as http]))

(def ^:dynamic config
  (ref
    {:jenkins-url ""
     :username ""
     :password ""}))

(defmacro with-config
  "Allows the user to pass in a config hash directly"
  [conf & body]
  `(binding [config (ref ~conf)]
    (do ~@body)))

(defn ^:private auth-info
  [conf]
  ((juxt :username :password) conf))

(defn ^:private request
  "Make a request to Jenkins
  - method http method i.e :get
  - url partial url i.e /api/json
  "
  [method url & params]
  (let [full-params
         (merge (or (first params) {})
           {:basic-auth (auth-info @config)
            :method method
            :accept :json })
        full-url (str (:jenkins-url @config) url)]
    (try
      (with-meta
        (-> (http/request (assoc full-params :url full-url :as :json))
        (:body)) {:method method :url url :params params})
    (catch Exception e (print e)))))

(defn normalize [v]
  (if (keyword? v) (name v) v))

(defn build
  "Will build a job on Jenkins"
  [job-name]
  (request :post
    (format "/job/%s/build" (normalize job-name))))
