# lein-ci

A Leiningen plugin that interacts with continuous integration services.

Only [Jenkins](http://jenkins-ci.org) is available right now.

## Usage

Put `[lein-ci "0.2.0"]` into the `:plugins` vector of your `project.clj`.

Example usage:

    $ lein ci build myproject-release

### Jenkins options

Set Jenkins-related options into the `:jenkins-ci` map of your `project.clj`.

Available options are the following:

* `:jenkins-url` **Required** the Jenkins server URL.

* `:username` **Required** the username used to connect to Jenkins API.

* `:password` **Required** the password or API token used to connect to Jenkins
  API.

Example:

```clojure
  ; project.clj
  (defproject myproject
    :description "My awesome project"
    :plugins [[lein-ci "0.2.0"]]
    :jenkins-ci {:jenkins-url "http://example.com/jenkins"
                 :username "jenkins"
                 :password "abc1234"})
```

## License

Copyright © 2014 Shoreware

Distributed under the Eclipse Public License, the same as Clojure.
