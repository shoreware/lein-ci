(defproject lein-ci "0.2.1-SNAPSHOT"
  :description "Leiningen plugin that interacts with continuous integration services."
  :url "https://bitbucket.org/shoreware/lein-ci"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[clj-http "1.0.1"]]
  :eval-in-leiningen true)
